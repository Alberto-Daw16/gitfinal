/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elecciones;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *Elecciones
 * @author jandri
 */
public class Elecciones {

    /**
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<String> provincias=new ArrayList<String>();
        Scanner sc=new Scanner(System.in);
        //provincias de España
        provincias.add("Comunidad Valenciana");
        provincias.add("Comunidad de Madrid");
        provincias.add("Castilla y León");
        provincias.add("Extremadura");
        provincias.add("Andalicía");
        provincias.add("Murcia");
        provincias.add("Catilla la Mancha");
        provincias.add("Aragón");
        provincias.add("Navarra");
        provincias.add("País Vasco");
        provincias.add("Cantabría");
        provincias.add("Asturias");
        provincias.add("La Rioja");
        provincias.add("Galicia");
        provincias.add("Las Baleares");
        provincias.add("Islas Canarias");
        //comprobar elección usuario
        boolean esta=false;
        do{
            System.out.println("Introduce la Comunidad en la que reside de España");
            String eleccion=sc.nextLine();
            Iterator<String> ite= provincias.iterator();
            while(ite.hasNext()){
                String comunidades=ite.next();
                if(eleccion.equals(comunidades)){
                    esta=true;
                }
            }
        }while(!esta);
    
    }
    
}
