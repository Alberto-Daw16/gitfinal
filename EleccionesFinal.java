/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eleccionesfinal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Alberto
 */
public class EleccionesFinal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "ISO-8859-1");
        boolean correcto = false;
        int edad = 0;
        String eleccion0 = "", siguientePersona;
        int azul = 0;
        int rojo = 0;
        int naranja = 0;
        int verde = 0;
        int morado = 0;
        int totalvotos = 0;
        //do while para que se repita todo si hay una siguiente persona
        do {
            //do while para segurarse que el usuaio pone Hombre o Mujer
            do {
                System.out.println("Dame tu sexo Hombre/Mujer");
                String sexo = sc.nextLine();
                if (sexo.equalsIgnoreCase("Hombre") || sexo.equalsIgnoreCase("Mujer")) {
                    correcto = true;
                }
            } while (!correcto);
            //do while para asegurarse de que el usuario mete un valor valido
            do {
                System.out.println("Introduce tu edad");
                String edad0 = sc.nextLine();
                try {
                    edad = Integer.parseInt(edad0);
                    correcto = true;
                } catch (NumberFormatException e) {
                    System.out.println("No has introducido un número");
                }
            } while (!correcto);
            //si es mayor de edad puede votar si no, no
            if (edad >= 18) {
                menuPartidos();
                eleccion0 = sc.nextLine();
                int eleccion = Integer.parseInt(eleccion0);
                //switch para aumentar los votos depende la eleccion del usuario
                switch (eleccion) {
                    case 1:
                        azul++;
                        totalvotos++;
                        break;
                    case 2:
                        rojo++;
                        totalvotos++;
                        break;
                    case 3:
                        naranja++;
                        totalvotos++;
                        break;
                    case 4:
                        verde++;
                        totalvotos++;
                        break;
                    case 5:
                        morado++;
                        totalvotos++;
                        break;
                    default:
                        totalvotos++;
                }
                ArrayList<String> provincias = new ArrayList<String>();

                //provincias de España
                anadirProvincias(provincias);

                //comprobar elección usuario
                boolean esta = false;
                //do while para asegurarse de que el usuario mete un valor valido
                do {
                    System.out.println("Introduce la Comunidad en la que reside de España");
                    System.out.println();
                    mostrarProvincias();
                    String seleccion = sc.nextLine();
                    Iterator<String> ite = provincias.iterator();
                    //while para recorrer el las comunidades
                    while (ite.hasNext()) {
                        String comunidades = ite.next();
                        if (seleccion.equals(comunidades)) {
                            esta = true;
                        }
                    }
                } while (!esta);

                System.out.println();
                System.out.println("Estos son los porcentajes de votos:");
                System.out.println("------------------------------------");
                System.out.println("Partido Popular : " + (azul * 100) / totalvotos + "%");
                System.out.println("Partido Socialista Obrero Español : " + (rojo * 100) / totalvotos + "%");
                System.out.println("Ciudadanos : " + (naranja * 100) / totalvotos + "%");
                System.out.println("VOX : " + (verde * 100) / totalvotos + "%");
                System.out.println("Podemos : " + (morado * 100) / totalvotos + "%");
                System.out.println("------------------------------------");

            } else {
                System.out.println("No puedes votar");
            }
            //do while para asegurar que el usuario pone un si o un no
            do {
                System.out.println("¿Hay siguiente persona para votar?");
                siguientePersona = sc.nextLine();
            } while (!(siguientePersona.equalsIgnoreCase("si") || siguientePersona.equalsIgnoreCase("no")));
        } while (siguientePersona.equalsIgnoreCase("si"));
    }

    /**
     * Metodo para mostrar el menu de los partidos
     */
    public static void menuPartidos() {
        System.out.println();
        System.out.println("Selecciona el partido al que vas a elegir");
        System.out.println("------------------------------------");
        System.out.println("1--Partido Popular");
        System.out.println("2--Partido Socialista Obrero Español");
        System.out.println("3--Ciudadanos");
        System.out.println("4--VOX");
        System.out.println("5--Podemos");
        System.out.println("------------------------------------");
    }

    /**
     * Metodo para añadir las provincias
     *
     * @param provincias
     */
    public static void anadirProvincias(ArrayList<String> provincias) {
        provincias.add("Comunidad Valenciana");
        provincias.add("Comunidad de Madrid");
        provincias.add("Castilla y León");
        provincias.add("Extremadura");
        provincias.add("Andalucía");
        provincias.add("Murcia");
        provincias.add("Catilla la Mancha");
        provincias.add("Aragón");
        provincias.add("Navarra");
        provincias.add("País Vasco");
        provincias.add("Cantabría");
        provincias.add("Asturias");
        provincias.add("La Rioja");
        provincias.add("Galicia");
        provincias.add("Las Baleares");
        provincias.add("Islas Canarias");
    }

    /**
     * Metodo para mostrar las provincias
     */
    public static void mostrarProvincias() {
        System.out.println("Estas son las opciones :");
        System.out.println("-------------------------");
        System.out.println("Comunidad Valenciana");
        System.out.println("Comunidad de Madrid");
        System.out.println("Castilla y León");
        System.out.println("Extremadura");
        System.out.println("Andalucía");
        System.out.println("Murcia");
        System.out.println("Catilla la Mancha");
        System.out.println("Aragón");
        System.out.println("Navarra");
        System.out.println("País Vasco");
        System.out.println("Cantabría");
        System.out.println("Asturias");
        System.out.println("La Rioja");
        System.out.println("Galicia");
        System.out.println("Las Baleares");
        System.out.println("Islas Canarias");
        System.out.println("-------------------------");
    }

}
